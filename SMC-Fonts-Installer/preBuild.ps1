﻿# Copyright (c) 2014 Ashik Salahudeen rdaneelolivaw@inflo.ws
# Copyright (c) Swathanthra Malayalam Computing, http://smc.org.in
#  All rights reserved.
#  
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#  
#  1. Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer. 
#  2. Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
#  The views and conclusions contained in the software and documentation are those
#  of the authors and should not be interpreted as representing official policies, 
#  either expressed or implied, of the Swathanthra Malayalam Computing Project
# 
#
#  This file downloads the fonts listed in the URL specified at $base_url 
#  This was written to be executed as pre build event for the SMC Fonts Installer

$base_url = 'http://smc.org.in/downloads/fonts'
$files = @{
  '..\..\fonts\Meera.ttf' = "$base_url/meera/Meera.ttf"
  '..\..\fonts\Rachana.ttf' = "$base_url/rachana/Rachana.ttf"
  '..\..\fonts\Rachana-Bold.ttf' ="$base_url/rachana/Rachana-Bold.ttf"
  '..\..\fonts\AnjaliOldLipi.ttf' = "$base_url/anjalioldlipi/AnjaliOldLipi.ttf"
  '..\..\fonts\Chilanka.ttf' = "$base_url/chilanka/Chilanka.ttf"
  '..\..\fonts\Dyuthi.ttf' = "$base_url/dyuthi/Dyuthi.ttf"
  '..\..\fonts\RaghuMalayalamSans.ttf' = "$base_url/raghumalayalamsans/RaghuMalayalamSans.ttf"
  '..\..\fonts\Suruma.ttf' = "$base_url/suruma/Suruma.ttf"
  '..\..\fonts\Keraleeyam.ttf' = "$base_url/keraleeyam/Keraleeyam.ttf"
}

$webclient = New-Object System.Net.WebClient

$files.GetEnumerator() | Foreach-Object {
  # Write-Output  $_.Key $_.Value
  $webclient.DownloadFile($_.Value, $_.Key) 
}
